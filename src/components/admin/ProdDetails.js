import {Fragment, useState, useEffect, useContext} from 'react';
import {Container, Row, Col, Card, Modal, Button} from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';

export default function ProdDetails () {

	// UseStates
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState("");
	const [type, setType] = useState("");
	const [image, setImage] = useState("");

	// Capture product's ID
	const {menuId} = useParams();

	// Model useStates
	const [show, setShow] = useState(false);

	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);

	// UseEffect to fetch product's details
	useEffect(() => {
		console.log(menuId);

		fetch('http://localhost:4000/product/:menuId')
		.then(result => result.json())
		.then(data => {
			// console.log(data);
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			setType(data.itemClass);
			setImage(data.image);
		})
		.catch(err => console.log(err));
	}, [menuId]);

	const update = (id) => {

		fetch(`${process.env.REACT_APP_CAPSTONE2}/product/edit/:menuId`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			// capture user's update request here
			body: JSON.stringify({
				name: name,
				description: description,
				price: price,
				itemClass: type,
				image: image
			})
		})
		.then(result => result.json())
		.then(data => {

			if(data){
				Swal.fire({
					title: 'Product updated!',
					icon: 'success'
				})
			} else {
				Swal.fire({
					title: 'Update failed.',
					text: 'See console for more info.',
					icon: 'error'
				})
			}
		})
		// .catch(err => console.log(err));
	};

	return(
		<Fragment>
			<Container className="mt-5">
	            <Row>
	                <Col lg={{ span: 6, offset: 3 }}>
	                    <Card>
	                        <Card.Body className="text-center">
	                            <Card.Title>{name}</Card.Title>
	                            <Card.Subtitle>Description:</Card.Subtitle>
	                            <Card.Text>{description}</Card.Text>
	                            <Card.Subtitle>Price:</Card.Subtitle>
	                            <Card.Text>PhP {price}</Card.Text>
	                            <Card.Subtitle>Image</Card.Subtitle>
	                            <Card.Text><img src = {image} id = "prod-det-img" className = "d-block"/></Card.Text>
	                            <Button variant="primary" onClick={handleShow}>
                                    Update Details
	                            </Button>
	                            <Button variant="primary" as = {Link} to = "/admin/allProducts">Back</Button>
	                        </Card.Body>        
	                    </Card>
	                </Col>
	            </Row>
	        </Container>
		</Fragment>
		)
}