import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import {useContext, useState} from 'react';
import {Fragment} from 'react';
import UserContext from '../UserContext.js';
import OrderContext from '../OrderContext.js';
import {Link, NavLink} from 'react-router-dom';
import Logo from '../images/AAGlogo-rev-hori.png';
import Cart from '../images/cart.png';

export default function AppNavBar (){

	// primary color: #15ab24
	const {user} = useContext(UserContext);

	const {order} = useContext(OrderContext);

	/*// UseStates for cart
	const [show, setShow] = useState(false);

	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);*/

	return(
		<Fragment>
			<Navbar bg="success" expand="lg" variant="dark" id = "navbar" className = "px-2 px-md-3 px-lg-4">
		        <Navbar.Brand as = {Link} to = "/"><img src={Logo} id = "navbarlogo"/></Navbar.Brand>
		        <Navbar.Toggle aria-controls="basic-navbar-nav" />
		        <Navbar.Collapse id="basic-navbar-nav">
			          <Nav className="me-auto">
			          	{/*Cart Access*/}
			          	{
			          		user && !user.isAdmin?
			          		<Nav.Link as = {Link} to = "/myOrder" className="cart-btn me-3"><img src={Cart} id = "navbarcart"/></Nav.Link>
			          		:
			          		null
			          	}	
			            <Nav.Link as = {NavLink} to = "/">Home</Nav.Link>		           
			            
			            {/*Admin Dashboard Access*/}
			            {
			            	user && user.isAdmin ?
			            		<NavDropdown title="Admin Controls" id="basic-nav-dropdown">
			            		  <NavDropdown.Item as = {NavLink} to = "/admin">Dashboard</NavDropdown.Item>
			            		  <NavDropdown.Divider />
			            		  <NavDropdown.Item as = {NavLink} to = "/admin/allOrders">All Orders</NavDropdown.Item>
			            		  <NavDropdown.Item as = {NavLink} to = "/admin/allProducts">Product Control</NavDropdown.Item>
			            		  <NavDropdown.Item as = {NavLink} to = "/admin/create">Create Product</NavDropdown.Item>
			            		  
			            		</NavDropdown>

			            	:
			            		null
			            		/*<Nav.Link as = {NavLink} to = "/order">Order Now</Nav.Link>*/
			            }

			              <Nav.Link as = {NavLink} to = "/menu">Full Menu</Nav.Link>

			            {/*Logout, Login, Register*/}
			            {
			            	user ?
			            	<Nav.Link as = {NavLink} to = "/logout">Logout</Nav.Link>
			            	:
			            	<Fragment>
			            		<Nav.Link as = {NavLink} to = "/register">Register</Nav.Link>
			            		<Nav.Link as = {NavLink} to = "/login">Login</Nav.Link>
			            	</Fragment>
			            }

			            <Nav.Link href="https://www.asiaambgracia.com/web/" target="_blank" className="ms-auto">
			                About Us
			            </Nav.Link>
			          </Nav>
		        </Navbar.Collapse>
		    </Navbar>
	    </Fragment>
		)
}