import {Fragment, useContext, useState, useEffect} from 'react';
import {Container, Card, Row, Col, Button, Modal, Form} from 'react-bootstrap';
import UserContext from '../UserContext.js';
import {Link} from 'react-router-dom';
import OrderContext from '../OrderContext.js';
import Swal from 'sweetalert2';

export default function MenuItems({prodProp}) {

	const {_id, name, description, price, itemClass, image} = prodProp;

	const [quantity, setQuantity] = useState("");

	const {user} = useContext(UserContext);
	const {order, setOrder, unSetOrder} = useContext(OrderContext);

	// UseStates for modal
	const [show, setShow] = useState(false);

	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);

	function newOrder ({_id}) {

		// {_id}.preventDefault();

		if(quantity <= 0){

			alert('Please select a valid number.')
			handleShow();
		} else {

			fetch(`${process.env.REACT_APP_CAPSTONE2}/order/${_id}`, {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
					quantity: quantity
				})
			})
			.then(result => result.json())
			.then(save => {

				if(save){

					setOrder(save);
					localStorage.setItem('order', JSON.stringify(save));

					Swal.fire({
						title: "Item added to cart!",
						icon: "success"
					})
					
					// navigate("")
				} else {
					Swal.fire({
						title: "Ordering error.",
						text: "See console for more info.",
						icon: "error"
					})
				}
			})

		}
	};



	return(
		<Fragment>
			<Col>
				<Card style={{ width: '18rem', height: '18rem'}} className = "mx-auto mt-2">
				      <Card.Img id = "user-menu-item-img" variant="top" src={image} className = "w-100"/>
				      <Card.Body>
				      	<Card.Title hidden>{_id}</Card.Title>
				        <Card.Title>{name} | {itemClass}</Card.Title>
				        <Card.Text id = "user-menu-price">
				          {price}
				        </Card.Text>
				        <Button variant="warning" onClick={() => setShow(true)}>
			                View Details
			            </Button>
				      </Card.Body>
		    	</Card>
	    	</Col>

	    	<Modal
	    		show={show}
	    		cancel={show.close}
	    	    size="lg"
	    	    aria-labelledby="contained-modal-title-vcenter"
	    	    centered
	    	    >
	    	      <Modal.Header>
	    	        <Modal.Title id="contained-modal-title-vcenter">
	    	          {name}
	    	        </Modal.Title>
	    	      </Modal.Header>
	    	      <Modal.Body>
	    	        <h4>Description</h4>
	    	        <p>
	    	          {description}
	    	        </p>
	    	        <p>
	    	        PhP {price}
	    	        </p>
	    	      </Modal.Body>
	    	      <Container>
		    	      <Form className = "col-1">
		    	      	<Form.Group className="mb-3" controlId="formBasicQuantity">
		    	      	        <Form.Control 
		    	      	        type="number" 
		    	      	        placeholder="0"
		    	      	        value={quantity}
		    	      	        onChange={(event)=>setQuantity(event.target.value)} />
	    	      	      </Form.Group>
		    	      </Form>
	    	      </Container>
	    	      <Modal.Footer>
	    	      	{
	    	      		user && !user.isAdmin
	    	      		?
	    	      		<Button variant = "success" onClick={(event)=>{setShow(false); newOrder({_id})}}>Add to cart</Button>
	    	      		:
	    	      		<Button variant = "success" onClick={(event)=>setShow(false)} disabled>Login as a user!</Button>

	    	      	}
	    	        <Button variant = "secondary" onClick={(event)=>setShow(false)}>Close</Button>
	    	      </Modal.Footer>
	    	    </Modal>
    	</Fragment>
		)
};