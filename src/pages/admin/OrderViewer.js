import {Fragment, useEffect, useState, useContext} from 'react';
import {Container, Row, Col, Card, Button, Table} from 'react-bootstrap';
import UserContext from '../../UserContext.js';
import {useNavigate} from 'react-router-dom';
// import OrderItems from '../../components/admin/OrderItems.js';
import Swal from 'sweetalert2';

export default function AllOrders () {

	const [orders, setOrders] = useState([]);

	const {user} = useContext(UserContext);
	const navigate = useNavigate();

	function test() {
		Swal.fire({
			title: "Coming soon!",
			text: "Function still in development.",
			icon: "info"
		})
	};

	// Default page display

	useEffect(() => {

		fetch(`${process.env.REACT_APP_CAPSTONE2}/order/kitchenOrders`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(result => result.json())
		.then(data => {
			setOrders(data.map(order => {

				const orderUserId = order.userId

				return(
					// <OrderItems key = {order._id} orderProp = {order}/>
						<Card  className = "my-3">
					      <Card.Header>
				      			Order: {order._id} | Status: {order.status}
					      </Card.Header>
					      <Card.Body>
					        	<Card.Title>User: {orderUserId}</Card.Title>
					        	<Card.Subtitle>Initial Transaction:</Card.Subtitle>
					        	<Card.Text>
					        	  {order.transactionStart}
					        	</Card.Text>
					        	<Card.Subtitle>End of Transaction:</Card.Subtitle>
					        	<Card.Text>
					        	  {order.purchaseDate}
					        	</Card.Text>
					        	<Card.Subtitle>Order:</Card.Subtitle>
					        	<Table>
					        		<thead>
					        			<tr>
					        				<th>Product Name</th>
					        				<th>Quantity</th>
					        				<th>Subtotal</th>
					        			</tr>
					        		</thead>
					        		<tbody>
					        		{order.ingredients.map((items, index) => {
					        			return(
					        				<tr key = {index}>
					        					<td>{items.productName}</td>
					        					<td>{items.quantity}</td>
					        					<td>{items.subTotal}</td>
					        				</tr>
					        				)
					        			})
					        		}
					        		</tbody>
					        	</Table>
					        	<Card.Subtitle>Total:</Card.Subtitle>
					        	<Card.Text>
					        	  {order.total}
					        	</Card.Text>
					        	<Card.Subtitle>Delivery Address:</Card.Subtitle>
					        	<Card.Text>
					        	  {order.address}
					        	</Card.Text>
					        	<Card.Subtitle>Special Instructions:</Card.Subtitle>
					        	<Card.Text>
					        	  {order.instructions}
					        	</Card.Text>
					        	<hr/>
					        	<Button 
					        		variant = "warning"
					        		className = "w-10 my-2 ml-1"
					        		onClick={/*({orderUserId}) => {console.log({orderUserId}); statusDelivery({orderUserId})}*/()=>test()}
					        		>
					        		Order is on the way
				        		</Button>
					      </Card.Body>
				      </Card>
					)
			}))
		})
		// .catch(err => console.log(err));
	}, []);

	// Functions for status updating buttons

	function statusDelivery({orderUserId}){
		console.log(orderUserId);
		fetch(`${process.env.REACT_APP_CAPSTONE2}/order/update1/${orderUserId}`, {
			method: 'PUT',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(result => result.json())
		.then(save => {
			console.log(save);
			if(save){
				Swal.fire({
					title: "Order status updated!",
					icon: "success"
				})
			} else {
				Swal.fire({
					title: "Status update failed.",
					icon: "error"
				})
			}
		})
	};

	function statusCompleted({orderUserId}){

		fetch(`${process.env.REACT_APP_CAPSTONE2}/order/update2/${orderUserId}`, {
			method: 'PUT',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(result => result.json())
		.then(save => {

			if(save){
				Swal.fire({
					title: "Order status updated!",
					icon: "success"
				})
			} else {
				console.log(save);
				Swal.fire({
					title: "Status update failed.",
					icon: "error"
				})
			}
		})
	};

	// Changing order viewing category



	return(
		user && user.isAdmin
		?
		<Fragment>
		<Container>
			<Row>
				<h1 className = "text-center mt-3">All Orders</h1>
				<Row>
					<Col>
						{orders}
					</Col>
				</Row>
			</Row>
		</Container>
		</Fragment>
		:
		navigate("*")
		)
};