import UserContext from '../../UserContext.js';
import {Container, Button, Form, Row, Col} from 'react-bootstrap';
import {Fragment, useEffect, useState, useContext} from 'react';
import {useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';

export default function CreateProd () {

	const {user} = useContext(UserContext);

	const navigate = useNavigate();

	// UseStates for form

	const [name, setName] = useState("");
	const [type, setType] = useState("");
	const [price, setPrice] = useState("");
	const [description, setDescription] = useState("");
	const [image, setImage] = useState("");

	const [isActive, setIsActive] = useState(false);

	// Button control
	useEffect(() => {
		if(name !== "" && type !== "" && price !== "" && description !== ""){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [name, type, price, description])

	function create (event) {
		event.preventDefault();

		fetch(`${process.env.REACT_APP_CAPSTONE2}/product/create`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price,
				itemClass: type,
				image: image
			})
		})
		.then(result => result.json())
		.then(data => {
			// console.log(data);
			if(data){
				Swal.fire({
					title: 'Your product has been created!',
					icon: 'success'
				});

				navigate("/admin/allProducts")
			} else {
				Swal.fire({
					title: 'Product not saved.',
					icon: 'error'
				});
			}
		})
		// .catch(err => console.log(err))
	};

	return(
		user && user.isAdmin
		?
		<Fragment>
		<Container>
			<Row>
				<h1 className = "text-center">Product Creation</h1>
				<Form className = "mt-3" onSubmit = {event => create(event)}>
					<Row>
						<Col>
							<Form.Group className="mb-3" controlId="formBasicProdName">
							  <Form.Label>Product Name:</Form.Label>
							  <Form.Control 
							  type="string" 
							  placeholder="Enter product name" 
							  value={name}
							  onChange = {event => setName(event.target.value)}
							  required
							  />
							</Form.Group>
						</Col>
						<Col>
							<Form.Group className="mb-3" controlId="formBasicType">
							  <Form.Label>Type:</Form.Label>
							  <Form.Control 
							  type="string" 
							  placeholder="Type either wok, toppings, or sauce in lowercase" 
							  value={type}
							  onChange = {event => setType(event.target.value)}
							  required
							  />
							</Form.Group>
						</Col>
					</Row>
				      
					<Row>
						<Form.Group className="mb-3" controlId="formBasicPrice">
						  <Form.Label>Price:</Form.Label>
						  <Form.Control 
						  type="number" 
						  placeholder="Set product price" 
						  value={price}
						  onChange = {event => setPrice(event.target.value)}
						  required
						  />
						</Form.Group>
					      <Form.Group className="mb-3" controlId="formBasicDescription">
					        <Form.Label>Description:</Form.Label>
					        <Form.Control 
					        as="textarea" 
					        rows={6} 
					        placeholder="Describe the product" 
					        value={description}
					        onChange = {event => setDescription(event.target.value)}
					        required
					        />
					      </Form.Group>
			      	</Row>
			      	<Row>
			      		<Form.Group className="mb-3" controlId="formBasicImage">
			      		  <Form.Label>Image URL:</Form.Label>
			      		  <Form.Control 
			      		  type="string"
			      		  placeholder="Paste image url here" 
			      		  value={image}
			      		  onChange = {event => setImage(event.target.value)}
			      		  />
			      		</Form.Group>
			      	</Row>
			      	{
			      		isActive ?
			      		<Button variant="primary" type="submit">
			      		  Submit
			      		</Button>
			      		:
			      		<Button variant="danger" disabled>
			      		  Submit
			      		</Button>
			      	}
			    </Form>
		    </Row>
		    </Container>
	    </Fragment>
	    :
	    navigate("*")
		)
}