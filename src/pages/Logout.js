import {Navigate} from 'react-router-dom';
import {useContext, useEffect} from 'react';
import UserContext from '../UserContext.js';
import OrderContext from '../OrderContext.js';

export default function Logout () {

	const {setUser, unSetUser} = useContext(UserContext);
	const {setOrder, unSetOrder} = useContext(OrderContext);

	useEffect(() => {
		unSetUser();
		setUser(null);
		unSetOrder();
		setOrder(null);
	}, []);

	return(
		<Navigate to = "/login"/>
		)
};